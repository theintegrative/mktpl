## 0.3.0 (2024-04-01)

### Feat

- **init**: initialize config file with content

### Fix

- add docstrings for better understanding the tool

### Refactor

- **init**: change ask to force parameter
- remove zellij config
- **init**: lessen code in cli

## 0.2.0 (2024-03-07)

### Feat

- **init**: add feature to initialize directories for mktpl
- **mktpl**: add intitial commit with project boilerplate
