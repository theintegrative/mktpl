import pytest


@pytest.fixture()
def init():
    # Default config dir config to initialize with force to skip prompts
    return ["init", "--force"]


@pytest.fixture()
def init_config_dir():
    # Custom config dir config to initialize with force to skip prompts
    return ["--config-dir", "/root/custom_dir", "--force"]
