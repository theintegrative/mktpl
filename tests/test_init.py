from typer.testing import CliRunner
from mktpl.main import app


runner = CliRunner()


def test_init_defaults(init):
    # Given nothing
    # When is mktpl init is executed
    # Then mktpl configs are created in the home directory
    result = runner.invoke(app, init)
    # assert result.stdout == 0
    assert result.exit_code == 0


def test_init_custom_dir(init, init_config_dir):
    # Given a custom directory
    # When mktpl is executed
    # Then mktpl configs are created in the custom directory
    result = runner.invoke(app, init + init_config_dir)
    # assert result.stdout == 0
    assert result.exit_code == 0
