import typer
from typing_extensions import Annotated
from mktpl import default
from mktpl import util


command = "init"
app = typer.Typer()


@app.callback(invoke_without_command=True)
def main(
    config_dir: str = default.config_dir,
    force: Annotated[bool, typer.Option("--force")] = False,
) -> None:
    """
    Initialize mktpl configuration files
    """
    parameters = locals()
    parameters.pop("force")
    if not force:
        config_dir = util.value_prompt(parameters)["config_dir"]
    default.init_config(config_dir, force)
