import pathlib
import os


def value_prompt(item: dict[str], func=None) -> dict[str]:
    for key, val in item.items():
        answer = input(f"{key}[{val}]: ")
        if answer:
            item[key] = answer
            print(item[key])
        if func:
            func(item[key])
    return item


def create_files(paths: dict[str]):
    for key, file in paths.items():
        path = pathlib.Path(file)
        path.touch(exist_ok=True)
        print(f"Created file {key}: {path}")


def create_dirs(paths: dict[str]):
    for key, directories in paths.items():
        path = pathlib.Path(directories)
        path.mkdir(parents=True, exist_ok=True)
        print(f"Created dir {key}: {path}")


def create_configs(dirs: dict[str], files: dict[str]):
    create_dirs(dirs)
    create_files(files)


def get_dicts(item: dict, keys: list[str]) -> tuple[dict[str], dict[str]]:
    positive = {}
    negative = {}
    for key, val in item.copy().items():
        if key in keys:
            positive[key] = val
            continue
        negative[key] = val
    return positive, negative


def pop(item, values: list[str]) -> str:
    [item.pop(value) for value in values]
    return item


def check_env_var(variable, default_value) -> str:
    """
    If value is not not set return default value
    Else return value from environment variable
    """
    if not (set_value := os.environ.get(variable)):
        return default_value
    return set_value


def set_env_var(variable, value):
    current_value = os.environ.get(variable)
    if current_value != value:
        bashrc_path = os.path.expanduser("~/.bashrc")
        with open(bashrc_path, "a+") as bashrc_file:
            bashrc_file.seek(0)
            export = f"export {variable}={value}"
            if export not in bashrc_file.read():
                bashrc_file.write(f"{export}\n")
        os.environ[variable] = value
        print(f"Environment variable '{variable}' set to '{value}'")
