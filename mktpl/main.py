import typer
from mktpl import init


command = "mktpl"
app = typer.Typer()
app.add_typer(init.app, name=init.command)


@app.callback()
def callback():
    """
    Make templates from files and directories, from everywhere.
    """
