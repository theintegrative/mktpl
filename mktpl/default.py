from pathlib import Path
from prompt_toolkit.shortcuts import confirm
from mktpl import util
from tomlkit import (
    comment,
    document,
    dumps,
)


config_dir = f"{Path.home()}/.config/mktpl"
config_dir_env_var = "MKTPL_CONFIG_DIR"


def write_toml(config_values, config_file):
    with open(config_file, "w") as config:
        config.write(dumps(config_values))


def override(config_file, force: bool = False) -> bool:
    path = Path(config_file)
    if not force and path.exists():
        if not path.stat().st_size == 0:
            print(f"{config_file} is not empty")
            return confirm("Override it?")
    return True


def init_config(config_dir: str = config_dir, force: bool = False) -> dict[str]:
    current_config_dir = util.check_env_var(config_dir_env_var, config_dir)
    util.set_env_var(config_dir_env_var, current_config_dir)
    config_file = f"{current_config_dir}/config.toml"
    if override(config_file, force):
        config = document()
        config.add(comment("Mktpl Configuration file"))
        config.add("mktpl_config_dir", current_config_dir)
        config.add("mktpl_config_file", f"{current_config_dir}/config.toml")
        config.add("mktpl_dir", f"{current_config_dir}/dir")
        config.add("mktpl_file", f"{current_config_dir}/file")
        config_files, config_dirs = util.get_dicts(config, ["mktpl_config_file"])
        util.create_configs(config_dirs, config_files)
        write_toml(config, config_file)
    exit(0)
