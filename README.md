# Make template (mktpl)

Make template is the tool for creating and using templates that consists of files and directories. Add files and directories as templates anywhere you are inside your terminal. 

## Feature idea's

You can find:
- [Features ideas to implement](https://gitlab.com/theintegrative/mktpl/-/wikis/Features-to-implement).
- [The structure of mktpl](https://gitlab.com/theintegrative/mktpl/-/wikis/Structure-of-mktpl)
