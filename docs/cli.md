# `mktpl`

Make templates from files and directories wherever you currently are.

**Usage**:

```console
$ mktpl [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--install-completion`: Install completion for the current shell.
* `--show-completion`: Show completion for the current shell, to copy it or customize the installation.
* `--help`: Show this message and exit.

**Commands**:

* `init`: Initialize mktpl configuration files

## `mktpl init`

Initialize mktpl configuration files

**Usage**:

```console
$ mktpl init [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--config-dir TEXT`: [default: /home/theintegrative/.config/mktpl]
* `--ask`
* `--help`: Show this message and exit.
