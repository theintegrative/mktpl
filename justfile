# Default chooser
default:
  @just --choose

# Generate cli docs
docs:
  typer mktpl.main utils docs --name mktpl --output docs/cli.md

# Format python code
fmt:
  ruff . --fix
  ruff format .
# Run all pre commit hooks
pre:
  pre-commit run -a

# Create a changelog
changelog:
  cz bump

# Build dockerfile test
build-test:
  docker build -f docker/Dockerfile.test -t test_mktpl .

# Run tests on code
test: build-test
  docker run test_mktpl

# Build dockerfile test
build-dev:
  docker build -f docker/Dockerfile.dev -t dev_mktpl .

# Automatically test when anything changes
auto: build-dev
  docker run -v $(pwd):/app -it dev_mktpl

# Test container ad hoc
ad-hoc:
  docker run -v $(pwd):/app -it test_mktpl sh
